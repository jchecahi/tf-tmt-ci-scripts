FROM registry.fedoraproject.org/fedora-minimal:39

RUN microdnf install -yq tmt python3-pyyaml python3-click
COPY scripts/generate-tf-pipeline.py /usr/local/bin
