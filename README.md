# tf-tmt-ci-scripts

This repository contains scripts and importable pipelines designed to enable
Continuous Integration (CI) in repositories featuring [Test Management Tool
(TMT)](https://tmt.readthedocs.io/en/stable/index.html) tests. The pipelines
leverage Red Hat's [Testing
Farm](https://docs.testing-farm.io/Testing%20Farm/0.1/index.html) to execute CI
jobs.

## testing-farm-pipeline

The `testing-farm-job.yml` is an importable pipeline that can be included into
any project containing TMT tests. It requires a custom-defined YAML file
specifying the test environments as its primary input. The pipeline executes a
job to analyze this file and generate a child pipeline. Subsequently, another
job triggers the child pipeline.

The child pipeline executes Testing Farm requests for each relevant environment,
ensuring that all TMT tests pass in those environments.

### Usage

To use the importable pipeline, obtaining a [Testing Farm API
key](https://docs.testing-farm.io/Testing%20Farm/0.1/onboarding.html) is
mandatory. Once you have an API key, export it to the CI environment by
following the [GitLab
documentation](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).
In your repository's GitLab web UI, navigate to Settings > CI/CD > Variables,
and add a variable. Set `TESTING_FARM_API_TOKEN` as the key and your API key as
the value. **Ensure to mask the variable to prevent exposure in logs.**

To add the pipeline to your project, use the `include:remote:` directive in your
local project's pipeline. Accepted input values include:

- `config-matrix`: Path to the configuration matrix in your repository.
- `trigger`: If set, the pipeline will use it to set the `trigger` [context
  dimension](https://tmt.readthedocs.io/en/stable/spec/context.html) when
  calling TMT. *Optional*.
- `runner-tag`: If set, it will set `default:tags:` in the child pipeline.
  `include:input:` does not support list types, [only string, numeric, or
  boolean](https://docs.gitlab.com/ee/ci/yaml/index.html#includeinputs), so
  **only one tag** is supported for now. *Optional*.

Example of enabling this in a merge-request pipeline in your `.gitlab-ci.yaml`
file:

```yaml
---
stages:
  # Your pipeline stages, if any
  - generate-test
  - child-trigger
  # more stages

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

# [Your pipeline jobs, if any]

include:
  - remote:
      'https://gitlab.com/jchecahi/tf-tmt-ci-scripts/-/raw/main/testing-farm-job.yml'
    inputs:
      config-matrix: '${CI_PROJECT_DIR}/path/to/config-matrix.yaml'
      trigger: custom-tmt-context-trigger
      runner-tag: custom-runner-tag
```

### Config matrix yaml format

The YAML describing the environments to test must follow this format:

```yaml
---
environments:
  - compose: TF_COMPOSE_NAME_1
    tmt-distro: TMT_CONTEXT_DISTRO_1
    archs: [arch1, arch2]
  - compose: TF_COMPOSE_NAME_2
    tmt-distro: TMT_CONTEXT_DISTRO_2
  # more environments
default_archs: [arch1, arch2, arch3]
```

Key details in the YAML include:
- `environments`: A list of dictionaries defining the environments where tests
  will run. Each element must contain:
  - `compose`: Name for the Testing Farm compose. Check [Testing Farm
    docs](https://docs.testing-farm.io/Testing%20Farm/0.1/test-environment.html#_composes)
    for correct names.
  - `tmt-distro`: TMT [context
    dimension](https://tmt.readthedocs.io/en/stable/spec/context.html) `distro`
    to pass when the pipeline calls `tmt`.
  - `archs`: Archs to test within the specified compose. This parameter is
    optional only if there is a `default_archs` list defined, in which case the
    values will be taken from there. If the `archs` key is defined, it always
    takes precedence over `default_archs`.
- `default_archs`: A list of default archs (e.g., `x86_64`, `aarch64`) for
  environments without explicit `archs`.
