#!/usr/bin/env python3

"""
Generate a dynamic pipeline based on an input yaml with distros and archs.

This script takes as input a yaml file that lists the environments as follows.
---
environments:
  - compose: TF_COMPOSE_NAME_1
    tmt-distro: TMT_CONTEXT_DISTRO_1
    archs: [arch1, arch2]
  - compose: TF_COMPOSE_NAME_2
    tmt-distro: TMT_CONTEXT_DISTRO_2
default_archs: [arch1, arch2, arch3]

If an element in the environments list has no `archs` key the script will populate
it with the contents of `default_archs` key

It produces a gitlab pipeline yaml consisting on parallel jobs that will
request testing-farm runs for each of the relevant environments.
"""

import sys
import yaml
import subprocess
import click
from dataclasses import dataclass, field


@dataclass
class CLIContext:
    """ Information about CLI invocation parameters """
    matrix_yaml: str
    trigger: str = ""
    runner_tags: list[str] = field(default_factory=list)


@click.pass_obj
def generate_tf_job(context: CLIContext) -> str:

    environments = get_relevant_envs()

    # If there are no relevant environments we still need to generate
    # some job to prevent the whole pipeline to fail. Related:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/215100
    if not environments:
        return generate_empty_job()

    job_yaml = {
        'stages': ['test'],
        'testing-farm-job': {
            'stage': 'test',
            'image': 'quay.io/testing-farm/cli:latest',
            'rules': [{'if': '$CI_PIPELINE_SOURCE == "parent_pipeline"'}],
            'script': [],
            'parallel': {
                'matrix': environments
            }
        }
    }

    script = (
        'testing-farm request --compose $COMPOSE --arch $ARCH'
        ' --git-ref $CI_MERGE_REQUEST_REF_PATH --git-url $CI_MERGE_REQUEST_PROJECT_URL'
        '  --context distro=$DISTRO --context arch=$ARCH'
    )

    if context.trigger:
        script += f" --context trigger={context.trigger}"

    job_yaml['testing-farm-job']['script'].append(script)

    if context.runner_tags:
        job_yaml['default'] = {'tags': context.runner_tags}

    return yaml.safe_dump(job_yaml)

@click.pass_obj
def generate_empty_job(context: CLIContext) -> str:
    job_yaml = {
        'stages': ['test'],
        'testing-farm-job': {
            'stage': 'test',
            'rules': [{'if': '$CI_PIPELINE_SOURCE == "parent_pipeline"'}],
            'script': ['echo "No relevant environments to test"']
        }
    }

    if context.runner_tags:
        job_yaml['default'] = {'tags': context.runner_tags}

    return yaml.safe_dump(job_yaml)

@click.pass_obj
def get_relevant_envs(context: CLIContext) -> list[dict]:
    """
    Return environments in which there are relevant tests
    """
    with open(context.matrix_yaml, 'r') as configfd:
        config = yaml.safe_load(configfd.read())

    # Do some simple parsing to the yaml contents
    if 'environments' not in config:
        raise KeyError(f"'environments' key not found in {context.matrix_yaml}")

    environments = []
    for distro in config['environments']:
        archs = distro['archs'] if 'archs' in distro else config['default_archs']
        for arch in archs:
            tmt_context = ['-c', f'arch={arch}', '-c', f'distro={distro["tmt-distro"]}']
            if context.trigger:
                tmt_context.extend(['-c', f'trigger={context.trigger}'])
            tmtrun = subprocess.run(
                ['tmt', *tmt_context, 'run','discover'],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.PIPE,
                text=True,
            )

            if "No tests found" in tmtrun.stderr:
                print(f"No tests relevant for distro {distro['tmt-distro']}:{arch}'")
                continue
            environments.append({
                "COMPOSE": distro['compose'],
                "DISTRO": distro['tmt-distro'],
                "ARCH": arch
            })
    return environments

@click.command()
@click.argument("matrix_yaml")
@click.option('-t','--trigger', default='', show_default=True,
              help="Value to pass to tmt as `trigger` context.")
@click.option('-r','--runner-tags', multiple=True,
              help="GitLab runner tags for the testing-farm job")
@click.pass_context
def main(click_context: click.Context, matrix_yaml, trigger, runner_tags):

    click_context.obj = CLIContext(matrix_yaml, trigger, runner_tags)

    with open('testing-farm-pipeline.yml', 'w') as outfile:
        outfile.write(generate_tf_job())


if __name__ == "__main__":
    main()
